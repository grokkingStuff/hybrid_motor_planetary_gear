#+TITLE: Spring



#+CAPTION: Mandrel Diameter
#+begin_src jupyter-python

# Kozo Hiraoka's SPRING WINDING MANDREL DIAMETER CALCULATION
# MANDREL.C   M. W. Klotz   2/05
# Computing mandrel size for spring winding.  Based on Kozo Hiraoka's article in
# "Home Shop Machinist", July/August 1987, pg. 30.

def mandrel_diameter(wire_diameter, spring_inner_diameter):

  # Given
  dw = wire_diameter            # wire diameter (in)
  id = spring_internal_diameter # spring ID (in)

  # Constants
  c0 = 0.980364                 # constant coefficient
  c1 = -0.012436                # first order coefficient

  # Calculated
  ds = id + dw                  # average spring diameter (in)
  fact = c0 + c1*ds/dw          # empirical factor (nd)
  dm = fact*ds - dw             # mandrel diameter (in)

  return dm
#+end_src




#+begin_src jupyter-python


# Dave Baker's SPRING CONSTANT CALCULATION


def spring_constant_calculation(od,d,l):
  """ Calculates the spring constant.
      Use mm units only

  Inputs
  od                            # Outside diameter of spring
  d                             # Wire diameter
  l                             # Compressed length of spring
  """
  import numpy as np

  D=od-d                        # mean diameter (mm)
  N=l/d-1.7                     # number of working coils
  k=np.power(d,4) / np.power(D,3) / N * 6e4 * 0.112985

  return k
#+end_src






* Gears


#+begin_src jupyter-python :exec no

NG = 2 # number of gears
N[NG] # teeth on gear */
P # diametral pitch */
phi # pressure angle (deg) */
gratio # gear ratio */
OD[NG] # outside diameter */
A[NG] # addendum */
D[NG] # dedendum */
W[NG] # whole depth */
CP[NG] # circular pitch */
T[NG] # tooth thickness */
PD[NG] # pitch diameter */
RB[NG] # base circle radius */
RP[NG] # tooth profile radius */
CD # center distance */


if ((fp=fopen(ofile,"wt")) == NULL)
	{printf ("FAILED TO OPEN OUTPUT FILE: %s\n",ofile); quit;}

vin ("Number of teeth on gear 1",&N[0],"lf",45.,"");
vin ("Number of teeth on gear 2",&N[1],"lf",20.,"");
vin ("Diametral Pitch (25.4/mod)",&P,"lf",20.,"");
vin ("Pressure Angle",&phi,"lf",20.,"deg");

gratio=MAX(N[0],N[1])/MIN(N[0],N[1]);
for (i=0 ; i<NG ; i++)
{
OD[i]=(N[i]+2.)/P;
A[i]=1.0/P;
D[i]=1.200/P;
W[i]=2.200/P;
CP[i]=PI/P;
T[i]=0.48*CP[i];
PD[i]=N[i]/P;
RB[i]=0.5*PD[i]*COSD(phi);
RP[i]=0.5*PD[i]*SIND(phi);
}
CD=0.5*(PD[0]+PD[1]);

fprintf (fp,"\nFOR BOTH GEARS:\n");
fprintf (fp,"Gear Ratio = %.6lf:1\n",gratio);
fprintf (fp,"Diametral Pitch = %.4lf\n",P);
fprintf (fp,"Pressure Angle = %.4lf deg\n",phi);
fprintf (fp,"Center Distance = %.4lf in = %.4lf mm\n",CD,CD*25.4);

for (i=0 ; i<NG ; i++)
{
fprintf (fp,"\nFOR GEAR %d:\n",i+1);
fprintf (fp,"Number of teeth = %.0lf\n",N[i]);
fprintf (fp,"Outside Diameter = %.4lf in = %.4lf mm\n",OD[i],OD[i]*25.4);
fprintf (fp,"Addendum = %.4lf in = %.4lf mm\n",A[i],A[i]*25.4);
fprintf (fp,"Dedendum = %.4lf in = %.4lf mm\n",D[i],D[i]*25.4);
fprintf (fp,"Whole Depth = %.4lf in = %.4lf mm\n",W[i],W[i]*25.4);
fprintf (fp,"Circular Pitch = %.4lf in = %.4lf mm\n",CP[i],CP[i]*25.4);
fprintf (fp,"Tooth Thickness = %.4lf in = %.4lf mm\n",T[i],T[i]*25.4);
fprintf (fp,"Pitch Diameter = %.4lf in = %.4lf mm\n",PD[i],PD[i]*25.4);
fprintf (fp,"Base Circle Radius = %.4lf in = %.4lf mm\n",RB[i],RB[i]*25.4);
fprintf (fp,"Tooth Profile Radius = %.4lf in = %.4lf mm\n",RP[i],RP[i]*25.4);
}


#+end_src
